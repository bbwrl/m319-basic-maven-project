
# Basic Maven Project
This is a very simple basic maven project for the very first steps in coding education.

## Content
The project contains:
- simple pom.xml
- JUnit Library for testing purpose
- a package (ch.bbw)
- a main application (MainApplication)

## Clone the project
```bash
git clone https://gitlab.com/bbwrl/m319-basic-maven-project.git
```

## contribute to the project
Teachers of the School BBW can
- contribute to the project by adding a pull request.
- contribute by writing a message to the author.
